# SAST Analyzer C# sample
| 起動 | SAST Analyzer | ジョブ名 | スキャナー |
| --- | --- | --- | --- |
| 有効 | [security-code-scan .NET analyzer](https://gitlab.com/gitlab-org/security-products/analyzers/security-code-scan) | `security-code-scan-sast` | [Security Code Scan](https://security-code-scan.github.io/) ([検査ルール](https://security-code-scan.github.io/#Rules)) |
| 有効 | [Semgrep analyzer](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep) | `semgrep-sast` | [Semgrep](https://semgrep.dev/) ([検査ルール](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/blob/main/rules/security_code_scan.yml)) |

SAST で検査した結果のレポートは[パイプライン](https://gitlab.com/sast-investigation/csharp-sast-sample/-/pipelines)からダウンロードしてください。


## サンプルファイル
* [VulnerabilityApp/VulnerabilityController.cs](./VulnerabilityApp/VulnerabilityController.cs) - 脆弱性のあるコントローラー。
* [VulnerabilityApp/NonVulnerabilityController.cs](./VulnerabilityApp/NonVulnerabilityController.cs) - Security Code Scan で脆弱性ではないとみなされるパターン。
  - API Controller は CSRF ありとみなしません。
  - 汚染されたデータが混入しない限り、コマンドインジェクションとしてみなしません。
  - Security Code Scan はコンパイルされたオブジェクトファイルをもとにコードを追いかけます。
* [VulnerabilityApp/IgnoreController.cs](./VulnerabilityApp/IgnoreController.cs) - 検出された脆弱性の無視。


## その他
### security-code-scan .NET analyzer
* 実行環境が Linux コンテナーなので .NET Framework への対応が不完全です。
* Visual Basic にも対応しています。
* [.NET で静的セキュリティコード検査 (Security Code Scan 編)](https://zenn.dev/masakura/articles/5d8525f9ffcf3e) も参考。


### Semgrep analyzer
* Security Code Scan のルールをもとに、GitLab 社が Semgrep ルールに移植したものです。
* Visual Basic には非対応です。
* .NET Framework にも対応していますが、ASP.NET WebForms などの .NET Framework 技術には対応していません。
* Semgrep の仕様により、Security Code Scan から[移植されていない検出ルール](https://gitlab.com/gitlab-org/secure/gsoc-sast-vulnerability-rules/playground/sast-rules#security-code-scan)があります。
